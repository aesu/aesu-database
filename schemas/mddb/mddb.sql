-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 89.108.103.91    Database: main
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admatrix`
--

DROP TABLE IF EXISTS `admatrix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admatrix` (
  `answer_id` int NOT NULL,
  `competence_id` int NOT NULL,
  `value` float NOT NULL,
  KEY `admatrix_competence_id_idx` (`competence_id`),
  KEY `admatrix_answer_id_idx` (`answer_id`),
  KEY `answer_id` (`answer_id`),
  CONSTRAINT `admatrix_answer_id` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `admatrix_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quest_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` smallint NOT NULL,
  `prime` tinyint(1) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `minvalue` smallint DEFAULT NULL,
  `maxvalue` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quest_answer_id_idx` (`quest_id`),
  CONSTRAINT `quest_answer_id` FOREIGN KEY (`quest_id`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4248 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basket`
--

DROP TABLE IF EXISTS `basket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `test_id` int NOT NULL,
  `timer` smallint DEFAULT NULL,
  `score` float DEFAULT NULL,
  `profile` text,
  `passed_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `basket_user_id_idx` (`user_id`),
  KEY `basket_test_id_idx` (`test_id`),
  CONSTRAINT `basket_test_id` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `basket_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2101 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `speciality_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `branch_speciality_id_idx` (`speciality_id`),
  CONSTRAINT `branch_speciality_id` FOREIGN KEY (`speciality_id`) REFERENCES `speciality` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `branchlinkdisc`
--

DROP TABLE IF EXISTS `branchlinkdisc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branchlinkdisc` (
  `branch_id` int NOT NULL,
  `discipline_id` int NOT NULL,
  PRIMARY KEY (`branch_id`,`discipline_id`),
  KEY `link_branch_id_idx` (`branch_id`),
  KEY `link_discipline_id_idx` (`discipline_id`),
  CONSTRAINT `blinkd_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blinkd_discipline_id` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `competence`
--

DROP TABLE IF EXISTS `competence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `shortname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `complinkmethod`
--

DROP TABLE IF EXISTS `complinkmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complinkmethod` (
  `competence_id` int NOT NULL,
  `method_id` int NOT NULL,
  PRIMARY KEY (`competence_id`,`method_id`),
  KEY `clinkm_method_id_idx` (`method_id`),
  CONSTRAINT `clinkm_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `clinkm_method_id` FOREIGN KEY (`method_id`) REFERENCES `method` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `complinktask`
--

DROP TABLE IF EXISTS `complinktask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complinktask` (
  `competence_id` int NOT NULL,
  `task_id` int NOT NULL,
  PRIMARY KEY (`competence_id`,`task_id`),
  KEY `clinkt_task_id_idx` (`task_id`),
  CONSTRAINT `clinkt_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `clinkt_task_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `connection`
--

DROP TABLE IF EXISTS `connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connection` (
  `connection_id` int NOT NULL AUTO_INCREMENT,
  `edelement_from_id` int NOT NULL,
  `edelement_to_id` int NOT NULL,
  `about` varchar(100) DEFAULT NULL,
  `position` int DEFAULT NULL,
  `external` smallint DEFAULT NULL,
  PRIMARY KEY (`connection_id`),
  KEY `connection_FK` (`edelement_from_id`),
  KEY `connection_FK_1` (`edelement_to_id`),
  CONSTRAINT `connection_FK` FOREIGN KEY (`edelement_from_id`) REFERENCES `edelement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `connection_FK_1` FOREIGN KEY (`edelement_to_id`) REFERENCES `edelement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=596 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `control`
--

DROP TABLE IF EXISTS `control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `core`
--

DROP TABLE IF EXISTS `core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic`
--

DROP TABLE IF EXISTS `didactic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `discipline_id` int NOT NULL,
  `position` smallint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `didactic_discipline_id_idx` (`discipline_id`),
  CONSTRAINT `didactic_discipline_id` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didacticdescript`
--

DROP TABLE IF EXISTS `didacticdescript`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didacticdescript` (
  `id` int NOT NULL,
  `type_id` int DEFAULT NULL,
  `importance` int DEFAULT NULL,
  `difficulty` int DEFAULT NULL,
  `core_id` int DEFAULT NULL,
  `viewtype` varchar(100) DEFAULT NULL,
  `packnumber` int DEFAULT NULL,
  `norms` text,
  `profile` text,
  `objnumber` tinyint DEFAULT NULL,
  `filestorage_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `didacticdescript_FK_2` (`type_id`),
  KEY `didacticdescript_FK_3` (`core_id`),
  KEY `didacticdescript_filestorage_FK` (`filestorage_id`),
  CONSTRAINT `didacticdescript_filestorage_FK` FOREIGN KEY (`filestorage_id`) REFERENCES `filestorage` (`id`) ON DELETE SET NULL,
  CONSTRAINT `didacticdescript_FK` FOREIGN KEY (`id`) REFERENCES `edelement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `didacticdescript_FK_2` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `didacticdescript_FK_3` FOREIGN KEY (`core_id`) REFERENCES `core` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didacticlinkcomp`
--

DROP TABLE IF EXISTS `didacticlinkcomp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didacticlinkcomp` (
  `didactic_id` int NOT NULL,
  `competence_id` int NOT NULL,
  `position` smallint NOT NULL,
  KEY `dlinkc_discipline_id_idx` (`didactic_id`),
  KEY `dlinkc_competence_id_idx` (`competence_id`),
  CONSTRAINT `dlinkc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dlinkc_didactic_id` FOREIGN KEY (`didactic_id`) REFERENCES `didactic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didacticlinkmethod`
--

DROP TABLE IF EXISTS `didacticlinkmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didacticlinkmethod` (
  `didactic_id` int NOT NULL,
  `method_id` int NOT NULL,
  `position` smallint NOT NULL,
  KEY `dlinkm_method_id_idx` (`method_id`),
  KEY `clinkm_didactic_id_idx` (`didactic_id`),
  CONSTRAINT `dlinkm_didactic_id` FOREIGN KEY (`didactic_id`) REFERENCES `didactic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dlinkm_method_id` FOREIGN KEY (`method_id`) REFERENCES `method` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didacticlinktask`
--

DROP TABLE IF EXISTS `didacticlinktask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didacticlinktask` (
  `didactic_id` int NOT NULL,
  `task_id` int NOT NULL,
  `position` smallint NOT NULL,
  KEY `dlinkt_discipline_id_idx` (`didactic_id`),
  KEY `dlinkt_task_id_idx` (`task_id`),
  CONSTRAINT `dlinkt_didactic_id` FOREIGN KEY (`didactic_id`) REFERENCES `didactic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dlinkt_task_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discform`
--

DROP TABLE IF EXISTS `discform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discform` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discipline`
--

DROP TABLE IF EXISTS `discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discipline` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `portrait` text,
  `owner` int NOT NULL,
  `shortname` varchar(15) NOT NULL,
  `status_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `disc_status_id_idx` (`status_id`),
  CONSTRAINT `disc_status_id` FOREIGN KEY (`status_id`) REFERENCES `disciplinestatus` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disciplinestatus`
--

DROP TABLE IF EXISTS `disciplinestatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplinestatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disclinkcomp`
--

DROP TABLE IF EXISTS `disclinkcomp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disclinkcomp` (
  `discipline_id` int NOT NULL,
  `competence_id` int NOT NULL,
  PRIMARY KEY (`discipline_id`,`competence_id`),
  KEY `disclinkcomp_competence_id_idx` (`competence_id`),
  CONSTRAINT `disclinkcomp_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `disclinkcomp_discipline_id` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `edelement`
--

DROP TABLE IF EXISTS `edelement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edelement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `level` tinyint NOT NULL,
  `order` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `name` text,
  `shortname` varchar(100) NOT NULL,
  `edelementtype_id` int DEFAULT NULL,
  `control_id` int DEFAULT NULL,
  `discform_id` int DEFAULT NULL,
  `branch_id` int DEFAULT NULL,
  `discipline_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `edelement_FK_3` (`discform_id`),
  KEY `edelement_FK_1` (`edelementtype_id`),
  KEY `edelement_FK` (`parent_id`),
  KEY `edelement_FK_2` (`control_id`),
  KEY `edelement_FK_5` (`branch_id`),
  KEY `edelement_FK_6` (`discipline_id`),
  CONSTRAINT `edelement_FK` FOREIGN KEY (`parent_id`) REFERENCES `edelement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `edelement_FK_1` FOREIGN KEY (`edelementtype_id`) REFERENCES `edelementtype` (`id`),
  CONSTRAINT `edelement_FK_2` FOREIGN KEY (`control_id`) REFERENCES `control` (`id`),
  CONSTRAINT `edelement_FK_3` FOREIGN KEY (`discform_id`) REFERENCES `discform` (`id`),
  CONSTRAINT `edelement_FK_5` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `edelement_FK_6` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=508 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `edelementlinkcomp`
--

DROP TABLE IF EXISTS `edelementlinkcomp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edelementlinkcomp` (
  `edelement_id` int NOT NULL,
  `competence_id` int NOT NULL,
  PRIMARY KEY (`edelement_id`,`competence_id`),
  KEY `edelementlinkcomp_FK_1` (`competence_id`),
  CONSTRAINT `edelementlinkcomp_FK` FOREIGN KEY (`edelement_id`) REFERENCES `edelement` (`id`),
  CONSTRAINT `edelementlinkcomp_FK_1` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `edelementlinkmethod`
--

DROP TABLE IF EXISTS `edelementlinkmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edelementlinkmethod` (
  `edelement_id` int NOT NULL,
  `method_id` int NOT NULL,
  PRIMARY KEY (`edelement_id`,`method_id`),
  KEY `edelementlinkmethod_FK_1` (`method_id`),
  CONSTRAINT `edelementlinkmethod_FK` FOREIGN KEY (`edelement_id`) REFERENCES `edelement` (`id`),
  CONSTRAINT `edelementlinkmethod_FK_1` FOREIGN KEY (`method_id`) REFERENCES `method` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `edelementlinkquest`
--

DROP TABLE IF EXISTS `edelementlinkquest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edelementlinkquest` (
  `edelement_id` int NOT NULL,
  `quest_id` int NOT NULL,
  `position` smallint DEFAULT NULL,
  `lms` tinyint DEFAULT NULL,
  KEY `questedelementlink_FK` (`edelement_id`),
  KEY `questedelementlink_FK_1` (`quest_id`),
  CONSTRAINT `questedelementlink_FK` FOREIGN KEY (`edelement_id`) REFERENCES `edelement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `questedelementlink_FK_1` FOREIGN KEY (`quest_id`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `edelementlinktask`
--

DROP TABLE IF EXISTS `edelementlinktask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edelementlinktask` (
  `edelement_id` int NOT NULL,
  `task_id` int NOT NULL,
  KEY `edelementlinktask_FK` (`edelement_id`),
  KEY `edelementlinktask_FK_1` (`task_id`),
  CONSTRAINT `edelementlinktask_FK` FOREIGN KEY (`edelement_id`) REFERENCES `edelement` (`id`),
  CONSTRAINT `edelementlinktask_FK_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `edelementtype`
--

DROP TABLE IF EXISTS `edelementtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edelementtype` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `typeText` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educationcourse`
--

DROP TABLE IF EXISTS `educationcourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educationcourse` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `about` text,
  `edelement_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `educationcourse_edelement_FK` (`edelement_id`),
  CONSTRAINT `educationcourse_edelement_FK` FOREIGN KEY (`edelement_id`) REFERENCES `edelement` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filestorage`
--

DROP TABLE IF EXISTS `filestorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filestorage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `path` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `haspermission`
--

DROP TABLE IF EXISTS `haspermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `haspermission` (
  `permission_id` int unsigned NOT NULL,
  `model_type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`user_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`user_id`,`model_type`),
  CONSTRAINT `haspermission_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hasrole`
--

DROP TABLE IF EXISTS `hasrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hasrole` (
  `role_id` int unsigned NOT NULL,
  `model_type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`user_id`,`model_type`),
  CONSTRAINT `hasrole_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `method`
--

DROP TABLE IF EXISTS `method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `method` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `discipline_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `method_discipline_id_idx` (`discipline_id`),
  CONSTRAINT `method_discipline_id` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `overdiscipline`
--

DROP TABLE IF EXISTS `overdiscipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `overdiscipline` (
  `id` int NOT NULL AUTO_INCREMENT,
  `branch_id` int NOT NULL COMMENT 'идентификатор факультативной группы',
  `user_id` int DEFAULT NULL COMMENT 'идентификатор пользователя, 0 - все пользователи в группе',
  `profile` text NOT NULL,
  `period_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `overdiscipline_branch_id_idx` (`branch_id`),
  CONSTRAINT `overdiscipline_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=765 DEFAULT CHARSET=utf8mb3 COMMENT='Надпредметная оценка УРК';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `definition` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `guard_name` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personalpreference`
--

DROP TABLE IF EXISTS `personalpreference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personalpreference` (
  `id` int NOT NULL AUTO_INCREMENT,
  `answer_id` int DEFAULT NULL,
  `edelement_id` int DEFAULT NULL,
  `competence_id` int DEFAULT NULL,
  `task_id` int DEFAULT NULL,
  `connectionType` int NOT NULL COMMENT '1 - edelement, 2 - competence, 3 - task',
  `compName` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `compShort` varchar(100) DEFAULT NULL,
  `edelemName` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `edelemShort` varchar(100) DEFAULT NULL,
  `compCode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `personalpreference_FK_1` (`edelement_id`),
  KEY `personalpreference_FK_2` (`competence_id`),
  KEY `personalpreference_FK_3` (`answer_id`),
  KEY `personalpreference_FK_4` (`task_id`),
  CONSTRAINT `personalpreference_FK` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`),
  CONSTRAINT `personalpreference_FK_1` FOREIGN KEY (`edelement_id`) REFERENCES `edelement` (`id`),
  CONSTRAINT `personalpreference_FK_2` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`),
  CONSTRAINT `personalpreference_FK_4` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=332 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quest`
--

DROP TABLE IF EXISTS `quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `type` smallint NOT NULL,
  `image` blob,
  PRIMARY KEY (`id`),
  KEY `quest_questtype_id_idx` (`type`),
  CONSTRAINT `quest_questtype_id` FOREIGN KEY (`type`) REFERENCES `questtype` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=673 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questlinkcomp`
--

DROP TABLE IF EXISTS `questlinkcomp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questlinkcomp` (
  `quest_id` int NOT NULL,
  `competence_id` int NOT NULL,
  PRIMARY KEY (`quest_id`,`competence_id`),
  KEY `questlinkcomp_competence_id_idx` (`competence_id`),
  CONSTRAINT `questlinkcomp_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `questlinkcomp_quest_id` FOREIGN KEY (`quest_id`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questtype`
--

DROP TABLE IF EXISTS `questtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questtype` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `type` smallint NOT NULL,
  `name` varchar(50) NOT NULL,
  `shortname` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `id` int NOT NULL AUTO_INCREMENT,
  `basket_id` int NOT NULL,
  `quest_id` int NOT NULL,
  `answer_id` int DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `result_basket_id_idx` (`basket_id`),
  KEY `result_quest_id_idx` (`quest_id`),
  KEY `result_answer_id` (`answer_id`),
  CONSTRAINT `result_answer_id` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `result_basket_id` FOREIGN KEY (`basket_id`) REFERENCES `basket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `result_quest_id` FOREIGN KEY (`quest_id`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100087 DEFAULT CHARSET=utf8mb3 COMMENT='результаты прохождения тестового бланка';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `guard_name` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rolehaspermission`
--

DROP TABLE IF EXISTS `rolehaspermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolehaspermission` (
  `permission_id` int unsigned NOT NULL,
  `role_id` int unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `rolehaspermission_role_id_foreign` (`role_id`),
  CONSTRAINT `rolehaspermission_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rolehaspermission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `speciality`
--

DROP TABLE IF EXISTS `speciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `speciality` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `shortname` varchar(15) NOT NULL,
  `about` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subgroup`
--

DROP TABLE IF EXISTS `subgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subgroup` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `portrait` text,
  `owner` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subgrouplinktest`
--

DROP TABLE IF EXISTS `subgrouplinktest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subgrouplinktest` (
  `subgroup_id` int NOT NULL,
  `test_id` int NOT NULL,
  PRIMARY KEY (`subgroup_id`,`test_id`),
  KEY `slinkt_test_id_idx` (`test_id`),
  CONSTRAINT `slinkt_subgroup_id` FOREIGN KEY (`subgroup_id`) REFERENCES `subgroup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `slinkt_test_id` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `discipline_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `task_discipline_id_idx` (`discipline_id`),
  CONSTRAINT `task_discipline_id` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `portrait` text,
  `discipline_id` int DEFAULT NULL,
  `timer` smallint DEFAULT NULL,
  `owner` int NOT NULL,
  `option` tinyint NOT NULL,
  `access_till` timestamp NOT NULL DEFAULT '2020-01-01 03:10:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `test_discipline_id_idx` (`discipline_id`),
  CONSTRAINT `test_discipline_id` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testlinkquest`
--

DROP TABLE IF EXISTS `testlinkquest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testlinkquest` (
  `test_id` int NOT NULL,
  `quest_id` int NOT NULL,
  `position` smallint NOT NULL,
  PRIMARY KEY (`test_id`,`quest_id`),
  KEY `test_id_idx` (`test_id`),
  KEY `quest_id_idx` (`quest_id`),
  CONSTRAINT `tlinkq_quest_id` FOREIGN KEY (`quest_id`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tlinkq_test_id` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `patronym` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `status_id` smallint NOT NULL DEFAULT '1',
  `branch_id` int DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_visit` timestamp NOT NULL DEFAULT '2020-01-01 07:10:00',
  PRIMARY KEY (`id`),
  KEY `status_id_idx` (`status_id`),
  KEY `user_branch_id_idx` (`branch_id`),
  CONSTRAINT `status_id` FOREIGN KEY (`status_id`) REFERENCES `userstatus` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `user_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=308 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_token` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_token_token_unique` (`token`),
  KEY `user_token_user_id_foreign` (`user_id`),
  CONSTRAINT `user_token_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userdigitalfootprint`
--

DROP TABLE IF EXISTS `userdigitalfootprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdigitalfootprint` (
  `id` int NOT NULL AUTO_INCREMENT,
  `usereducationprofile_id` int NOT NULL,
  `page_from` varchar(100) DEFAULT NULL,
  `page_to` varchar(100) DEFAULT NULL,
  `page_component` varchar(100) DEFAULT NULL,
  `actiondfp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `parameters` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userdigitalfootprint_usereducationprofile_FK` (`usereducationprofile_id`),
  CONSTRAINT `userdigitalfootprint_usereducationprofile_FK` FOREIGN KEY (`usereducationprofile_id`) REFERENCES `usereducationprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usereducationcourse`
--

DROP TABLE IF EXISTS `usereducationcourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usereducationcourse` (
  `id` int NOT NULL AUTO_INCREMENT,
  `usereducationprofile_id` int NOT NULL,
  `current_edelement_id` int DEFAULT NULL,
  `score` float DEFAULT NULL,
  `educationcourse_id` int NOT NULL,
  `finished` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usereducationcourse_usereducationprofile_FK` (`usereducationprofile_id`),
  KEY `usereducationcourse_edelement_FK` (`current_edelement_id`),
  KEY `usereducationcourse_educationcourse_FK` (`educationcourse_id`),
  CONSTRAINT `usereducationcourse_edelement_FK` FOREIGN KEY (`current_edelement_id`) REFERENCES `edelement` (`id`),
  CONSTRAINT `usereducationcourse_educationcourse_FK` FOREIGN KEY (`educationcourse_id`) REFERENCES `educationcourse` (`id`),
  CONSTRAINT `usereducationcourse_usereducationprofile_FK` FOREIGN KEY (`usereducationprofile_id`) REFERENCES `usereducationprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usereducationprofile`
--

DROP TABLE IF EXISTS `usereducationprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usereducationprofile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `edelement_id` int NOT NULL,
  `started_at` date DEFAULT NULL,
  `finished_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usereducationprofile_edelement_FK` (`edelement_id`),
  KEY `usereducationprofile_user_FK` (`user_id`),
  CONSTRAINT `usereducationprofile_edelement_FK` FOREIGN KEY (`edelement_id`) REFERENCES `edelement` (`id`),
  CONSTRAINT `usereducationprofile_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userlinksubgroup`
--

DROP TABLE IF EXISTS `userlinksubgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userlinksubgroup` (
  `user_id` int NOT NULL,
  `subgroup_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`subgroup_id`),
  KEY `ulinkg_subgroup_id_idx` (`subgroup_id`),
  CONSTRAINT `ulinkg_subgroup_id` FOREIGN KEY (`subgroup_id`) REFERENCES `subgroup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ulinkg_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userstatus`
--

DROP TABLE IF EXISTS `userstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userstatus` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `order` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'main'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-10-26 12:59:10
